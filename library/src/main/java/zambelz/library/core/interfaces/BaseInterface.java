package zambelz.library.core.interfaces;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import zambelz.library.core.components.ActivityComponent;
import zambelz.library.core.components.MainApplicationComponent;
import zambelz.library.core.modules.ActivityModule;
import zambelz.library.core.modules.FragmentModule;

/**
 * Created by nandajulianda on 11/24/16.
 */

public interface BaseInterface {
    void initializeInjector();
    void initialContent();

    void addSubscription(Subscription subscription);
    void addSubscription(Subscription... subscriptions);
    CompositeSubscription getSubscriptions();

    ActivityComponent getActivityComponent();
    MainApplicationComponent getMainApplicationComponent();
    ActivityModule getActivityModule();
    FragmentModule getFragmentModule();
}
