package zambelz.library.core.interfaces;

/**
 * Created by nandajulianda on 11/26/16.
 */

public interface ComponentIntegrator<T> {
    T getComponent();
}
