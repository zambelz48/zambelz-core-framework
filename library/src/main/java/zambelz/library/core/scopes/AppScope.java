package zambelz.library.core.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by nandajulianda on 11/23/16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {}
