package zambelz.library.core.modules;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import zambelz.library.core.BuildConfig;

/**
 * Created by nandajulianda on 11/26/16.
 */

@Module
public class OkHttpClientModule {

    public OkHttpClientModule() {}

    @Provides @Singleton
    Cache provideDefaultHttpCache(Context context) {
        int cacheSize = 10 * 1024 * 1024;
        return new Cache(context.getCacheDir(), cacheSize);
    }

    @Provides @Singleton
    OkHttpClient provideOkHttpClient(Cache cache) {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        if(BuildConfig.DEBUG) {
            client.addNetworkInterceptor(new StethoInterceptor());
        }
        client.readTimeout(300, TimeUnit.SECONDS);
        client.writeTimeout(300, TimeUnit.SECONDS);
        client.connectTimeout(300, TimeUnit.SECONDS);
        client.cache(cache);
        return client.build();
    }

}
