package zambelz.library.core.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import zambelz.library.core.config.AppConfig;
import zambelz.library.core.utilities.SharedPreferenceLoader;

/**
 * Created by nandajulianda on 11/20/16.
 */

@Module
public class MainApplicationModule {

    private final Application application;

    public MainApplicationModule(Application application) {
        this.application = application;
    }

    @Provides @Singleton
    Context provideApplication() {
        return this.application;
    }

    @Provides @Singleton
    AppConfig provideAppConfig(Context context) {
        return new AppConfig(context);
    }

    @Provides @Singleton
    SharedPreferenceLoader provideSharedPreferenceLoader(Context context) {
        return new SharedPreferenceLoader(context);
    }

    @Deprecated /** use SharedPreferenceLoader instead */
    @Provides @Singleton
    SharedPreferences providesSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
