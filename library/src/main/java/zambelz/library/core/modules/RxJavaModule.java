package zambelz.library.core.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by nandajulianda on 11/26/16.
 */

@Module
public class RxJavaModule {

    public RxJavaModule() {}

    @Provides @Singleton
    RxJavaCallAdapterFactory provideRxJavaCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

}
