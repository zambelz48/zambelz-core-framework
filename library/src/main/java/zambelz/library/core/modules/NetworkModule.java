package zambelz.library.core.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import zambelz.library.core.utilities.EmptyResponseConverterFactory;

/**
 * Created by nandajulianda on 11/26/16.
 */

@Module
public class NetworkModule {

    private final String baseUrl;

    public NetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides @Singleton
    Retrofit.Builder retrofitBuilder() {
        return new Retrofit.Builder();
    }

    @Provides @Singleton
    EmptyResponseConverterFactory provideEmptyResponseConverterFactory() {
        return new EmptyResponseConverterFactory();
    }

    @Provides @Singleton
    Retrofit provideRetrofit(Retrofit.Builder retrofitBuilder,
                             OkHttpClient okHttpClient,
                             GsonConverterFactory gsonConverterFactory,
                             EmptyResponseConverterFactory emptyResponseConverterFactory,
                             RxJavaCallAdapterFactory rxAdapter) {

        return retrofitBuilder
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(emptyResponseConverterFactory)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxAdapter)
                .build();
    }

}
