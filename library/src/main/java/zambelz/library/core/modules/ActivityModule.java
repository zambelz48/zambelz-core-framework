package zambelz.library.core.modules;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import zambelz.library.core.scopes.AppScope;

/**
 * Created by nandajulianda on 11/20/16.
 */

@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides @AppScope
    Activity provideActivity() {
        return this.activity;
    }

    @Provides @AppScope
    AppCompatActivity provideAppCompatActivity() {
        return (AppCompatActivity) this.activity;
    }

}
