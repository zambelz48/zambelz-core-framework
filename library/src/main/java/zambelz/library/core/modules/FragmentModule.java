package zambelz.library.core.modules;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import zambelz.library.core.scopes.AppScope;

import static zambelz.library.core.config.Constant.CHILD_FRAGMENT_MANAGER;
import static zambelz.library.core.config.Constant.PARENT_FRAGMENT_MANAGER;
import static zambelz.library.core.config.Constant.SUPPORT_FRAGMENT_MANAGER;
/**
 * Created by nandajulianda on 11/22/16.
 */

@Module
public class FragmentModule {

    private Fragment fragment;

    public FragmentModule() {}

    public FragmentModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides @AppScope
    Fragment provideFragment() {
        return this.fragment;
    }

    @Provides @AppScope
    @Named(PARENT_FRAGMENT_MANAGER)
    FragmentManager provideFragmentManager() {
        return this.fragment.getFragmentManager();
    }

    @Provides @AppScope
    @Named(CHILD_FRAGMENT_MANAGER)
    FragmentManager provideChildFragmentManager() {
        return this.fragment.getChildFragmentManager();
    }

    @Provides @AppScope
    @Named(SUPPORT_FRAGMENT_MANAGER)
    FragmentManager provideSupportFragmentManager(Activity activity) {
        return ((AppCompatActivity) activity).getSupportFragmentManager();
    }

}
