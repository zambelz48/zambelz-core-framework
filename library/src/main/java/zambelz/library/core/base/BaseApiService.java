package zambelz.library.core.base;

import retrofit2.Retrofit;

/**
 * Created by nandajulianda on 12/6/16.
 */

public abstract class BaseApiService {

    protected final Retrofit retrofit;

    protected BaseApiService(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    protected <T> T createNetworkRequest(Class<T> service) {
        return retrofit.create(service);
    }

}
