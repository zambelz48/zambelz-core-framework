package zambelz.library.core.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import zambelz.library.core.MainApplication;
import zambelz.library.core.components.ActivityComponent;
import zambelz.library.core.components.DaggerActivityComponent;
import zambelz.library.core.components.MainApplicationComponent;
import zambelz.library.core.interfaces.BaseInterface;
import zambelz.library.core.modules.ActivityModule;
import zambelz.library.core.modules.FragmentModule;

/**
 * Created by nandajulianda on 11/19/16.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseInterface {

    private CompositeSubscription compositeSubscription;

    public Intent getCallingIntent(Context context, @Nullable Object... params) {
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initializeInjector();
        initialContent();
    }

    @Override
    public void initializeInjector() {}

    @Override
    public void initialContent() {}

    @Override
    public void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    @Override
    public void addSubscription(Subscription... subscriptions) {
        compositeSubscription.addAll(subscriptions);
    }

    @Override
    public CompositeSubscription getSubscriptions() {
        return compositeSubscription;
    }

    @Override
    public ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder()
                .mainApplicationComponent(getMainApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public MainApplicationComponent getMainApplicationComponent() {
        return ((MainApplication) getApplication()).getMainApplicationComponent();
    }

    @Override
    public ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    @Override
    public FragmentModule getFragmentModule() {
        return new FragmentModule();
    }

    @Override
    protected void onDestroy() {
        compositeSubscription.unsubscribe();
        super.onDestroy();
    }

}
