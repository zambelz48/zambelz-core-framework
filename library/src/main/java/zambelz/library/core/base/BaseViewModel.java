package zambelz.library.core.base;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by nandajulianda on 11/28/16.
 */

public abstract class BaseViewModel<VMListener> {

    protected final VMListener vmListener;

    protected interface ErrorBodyResponseListener {
        void onHttpErrorResponse(int code, JSONObject errorObject) throws JSONException;
    }

    protected BaseViewModel(VMListener vmListener) {
        this.vmListener = vmListener;
    }

    protected <T> Observable.Transformer<T, T> defaultScheduler() {
        return observable -> observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    protected void handleHttpErrorBodyResponse(Throwable error, ErrorBodyResponseListener errorListener) {
        try {
            if(error instanceof HttpException) {
                int httpErrorCode = ((HttpException) error).code();
                ResponseBody body = ((HttpException) error).response().errorBody();

                JSONObject bodyMessage = new JSONObject(body.source().buffer().readUtf8());
                errorListener.onHttpErrorResponse(httpErrorCode, bodyMessage);
            } else {
                errorListener.onHttpErrorResponse(0, null);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

}
