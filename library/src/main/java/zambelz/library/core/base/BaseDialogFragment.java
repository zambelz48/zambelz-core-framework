package zambelz.library.core.base;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import zambelz.library.core.MainApplication;
import zambelz.library.core.components.ActivityComponent;
import zambelz.library.core.components.DaggerActivityComponent;
import zambelz.library.core.components.MainApplicationComponent;
import zambelz.library.core.interfaces.BaseInterface;
import zambelz.library.core.modules.ActivityModule;
import zambelz.library.core.modules.FragmentModule;

/**
 * Created by nandajulianda on 1/8/17.
 */

public abstract class BaseDialogFragment extends DialogFragment implements BaseInterface {

    private CompositeSubscription compositeSubscription;

    protected abstract String dialogTag();

    public void showDialog(FragmentManager manager) {
        show(manager, dialogTag());
    }

    public void dismissDialog() {
        dismiss();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initializeInjector();
        initialContent();
    }

    @Override
    public void initializeInjector() {}

    @Override
    public void initialContent() {}

    @Override
    public void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    @Override
    public void addSubscription(Subscription... subscriptions) {
        compositeSubscription.addAll(subscriptions);
    }

    @Override
    public CompositeSubscription getSubscriptions() {
        return compositeSubscription;
    }

    @Override
    public ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder()
                .mainApplicationComponent(getMainApplicationComponent())
                .activityModule(getActivityModule())
                .build();
    }

    @Override
    public MainApplicationComponent getMainApplicationComponent() {
        return ((MainApplication) getActivity().getApplication()).getMainApplicationComponent();
    }

    @Override
    public ActivityModule getActivityModule() {
        return new ActivityModule(getActivity());
    }

    @Override
    public FragmentModule getFragmentModule() {
        return new FragmentModule(this);
    }

    @Override
    public void onDestroy() {
        compositeSubscription.unsubscribe();
        super.onDestroy();
    }

}
