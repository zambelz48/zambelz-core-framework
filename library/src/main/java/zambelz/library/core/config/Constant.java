package zambelz.library.core.config;

/**
 * Created by nandajulianda on 11/22/16.
 */

public final class Constant {

    public static final String PARENT_FRAGMENT_MANAGER = "parentFragmentManager";
    public static final String CHILD_FRAGMENT_MANAGER = "childFragmentManager";
    public static final String SUPPORT_FRAGMENT_MANAGER = "supportFragmentManager";

    private Constant(){}

}
