package zambelz.library.core.config;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by nandajulianda on 11/26/16.
 */

public final class AppConfig {

    private final Context context;
    private Properties properties;

    public AppConfig(Context context) {
        this.context = context;
        loadAppConfigFile();
    }

    private void loadAppConfigFile() {
        try {
            properties = new Properties();
            InputStream inputStream = context.getAssets().open("AppConfig.properties");
            properties.load(inputStream);
        } catch (IOException e){
            System.out.print(e.getMessage());
        }
    }

    public String getProp(String key) {
        return properties.getProperty(key);
    }

    public boolean isWriteLogGranted() {
        return getProp("WRITE_LOG_TO_STORAGE").equals("yes");
    }

    public String getApiBaseUrl() {
        return getProp("API_BASE_URL");
    }

    public String getAppDataDir() {
        return getProp("APP_DATA_DIR");
    }

    public String getLogsDir() {
        return getProp("APP_DATA_DIR") + "Logs/";
    }

}
