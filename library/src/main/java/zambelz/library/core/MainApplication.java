package zambelz.library.core;

import android.app.Application;

import com.facebook.stetho.Stetho;

import zambelz.library.core.components.DaggerMainApplicationComponent;
import zambelz.library.core.components.MainApplicationComponent;
import zambelz.library.core.config.AppConfig;
import zambelz.library.core.modules.GsonModule;
import zambelz.library.core.modules.MainApplicationModule;
import zambelz.library.core.modules.NetworkModule;
import zambelz.library.core.modules.OkHttpClientModule;
import zambelz.library.core.modules.RxJavaModule;
import zambelz.library.core.utilities.LoggerUtils;

/**
 * Created by nandajulianda on 11/19/16.
 */

public class MainApplication extends Application {

    private AppConfig appConfig;
    private MainApplicationComponent mainApplicationComponent;
    private Thread.UncaughtExceptionHandler defaultUEH;

    Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    if(appConfig.isWriteLogGranted()) {
                        LoggerUtils.writeErrorLog(appConfig.getLogsDir(), ex.getMessage());
                    }
                    defaultUEH.uncaughtException(thread, ex);
                }
            };

    public MainApplication() {
        defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appConfig = new AppConfig(getApplicationContext());

        this.initializeMainApplicationComponent();

        if(BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
    }

    private void initializeMainApplicationComponent() {
        DaggerMainApplicationComponent.Builder mainAppBuilder = DaggerMainApplicationComponent.builder();

        mainAppBuilder.mainApplicationModule(new MainApplicationModule(this));
        mainAppBuilder.okHttpClientModule(getOkHttpClientModule());
        mainAppBuilder.gsonModule(getGsonModule());
        mainAppBuilder.rxJavaModule(getRxJavaModule());
        mainAppBuilder.networkModule(getNetworkModule(appConfig.getApiBaseUrl()));

        this.mainApplicationComponent = mainAppBuilder.build();
    }

    public MainApplicationComponent getMainApplicationComponent() {
        return this.mainApplicationComponent;
    }

    public OkHttpClientModule getOkHttpClientModule() {
        return new OkHttpClientModule();
    }

    public GsonModule getGsonModule() {
        return new GsonModule();
    }

    public RxJavaModule getRxJavaModule() {
        return new RxJavaModule();
    }

    public NetworkModule getNetworkModule(String baseUrl) {
        return new NetworkModule(baseUrl);
    }

}
