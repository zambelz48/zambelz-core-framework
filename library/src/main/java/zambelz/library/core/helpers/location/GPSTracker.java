package zambelz.library.core.helpers.location;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import javax.inject.Inject;

import zambelz.library.core.scopes.AppScope;

/**
 * Created by nandajulianda on 12/19/16.
 */

@AppScope
@SuppressWarnings("MissingPermission")
public class GPSTracker extends Service implements LocationListener {

    private static final String TAG = "GPSTracker";
    private final Context context;
    private Location location;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    private double latitude = 0.0;
    private double longitude = 0.0;

    private static final long MIN_DISTANCES_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10;

    private LocationManager locationManager;

    @Inject public GPSTracker(@NonNull Context context) {
        this.context = context;
        getCurrentLocation();
    }

    public Location getCurrentLocation() {
        if (!isPermissionGranted()) {
            return null;
        }
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        requestLocationUsingGps();
        requestLocationUsingNetworkProvider();
        return location;
    }

    private void requestLocationUsingGps() {
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPSEnabled) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCES_CHANGE_FOR_UPDATES, this);

            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
        }
    }

    private void requestLocationUsingNetworkProvider() {
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (isNetworkEnabled) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCES_CHANGE_FOR_UPDATES, this);

            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
        }
    }

    public boolean isPermissionGranted() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void stopUsingGPS() {
        if (locationManager != null) {
            if(!isPermissionGranted()) {
                return;
            }
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    public double getLatitude() {
        if(location != null) {
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude() {
        if(location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public boolean canGetLocation() {
        return isGPSEnabled || isNetworkEnabled;
    }

    public void showSettingsAlert() {
        new AlertDialog.Builder(context)
                .setTitle("Caution !")
                .setMessage("GPS is not enabled. do you want to enable it on the setting ?")
                .setCancelable(false)
                .setPositiveButton("Go To Settings", (dialog, i) -> {
                    dialog.dismiss();
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                })
                .setNegativeButton("Cancel", (dialog, i) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onLocationChanged(Location arg0) {
        Log.d(TAG, "location changed");
    }

    @Override
    public void onProviderDisabled(String arg0) {
        Log.d(TAG, "provider disabled");
    }

    @Override
    public void onProviderEnabled(String arg0) {
        Log.d(TAG, "provider enabled");
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        Log.d(TAG, "Status changed");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
