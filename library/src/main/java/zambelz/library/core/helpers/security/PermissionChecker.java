package zambelz.library.core.helpers.security;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import javax.inject.Inject;

import zambelz.library.core.scopes.AppScope;

/**
 * Created by nandajulianda on 12/20/16.
 */

@AppScope
public class PermissionChecker {

    public interface Callback {
        void onPermissionAlreadyGranted();
    }

    private final Activity activity;

    @Inject public PermissionChecker(Activity activity) {
        this.activity = activity;
    }

    public void checkPermission(@NonNull String permissionType, int reqCode, PermissionChecker.Callback callback) {
        if (!isPermissionGranted(activity, permissionType)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionType)) {
                requestPermission(permissionType, reqCode);
            } else {
                requestPermission(permissionType, reqCode);
            }
        } else {
            callback.onPermissionAlreadyGranted();
        }
    }

    public boolean isPermissionGranted(Context context, @NonNull String permissionType) {
        return ContextCompat.checkSelfPermission(context, permissionType) == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isPermissionResultGranted(@NonNull int[] grantResults) {
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission(@NonNull String permissionType, int reqCode) {
        ActivityCompat.requestPermissions(activity, new String[]{ permissionType }, reqCode);
    }

}
