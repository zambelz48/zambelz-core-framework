package zambelz.library.core.helpers.security;

import android.Manifest;

/**
 * Created by nandajulianda on 12/20/16.
 */

public final class PermissionRequestCode {

    /**
     * Manifest.permission.ACCESS_NETWORK_STATE
     */
    public static final int REQCODE_ACCESS_NETWORK_STATE = 10;

    /**
     * Manifest.permission.INTERNET
     */
    public static final int REQCODE_ACCESS_INTERNET = 11;

    /**
     * Manifest.permission.ACCESS_COARSE_LOCATION
     */
    public static final int REQCODE_ACCESS_COARSE_LOCATION = 12;

    /**
     * Manifest.permission.ACCESS_FINE_LOCATION
     */
    public static final int REQCODE_ACCESS_FINE_LOCATION = 13;

    /**
     * Manifest.permission.READ_EXTERNAL_STORAGE
     */
    public static final int REQCODE_READ_EXTERNAL_STORAGE = 14;

    /**
     * Manifest.permission.WRITE_EXTERNAL_STORAGE
     */
    public static final int REQCODE_WRITE_EXTERNAL_STORAGE = 15;

    /**
     * Manifest.permission.CAMERA
     */
    public static final int REQCODE_CAMERA = 16;

}
