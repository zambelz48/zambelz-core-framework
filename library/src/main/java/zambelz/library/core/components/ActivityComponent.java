package zambelz.library.core.components;

import android.app.Activity;

import dagger.Component;
import zambelz.library.core.base.BaseActivity;
import zambelz.library.core.base.BaseDialogFragment;
import zambelz.library.core.base.BaseFragment;
import zambelz.library.core.modules.ActivityModule;
import zambelz.library.core.scopes.AppScope;

/**
 * Created by nandajulianda on 11/23/16.
 */

@AppScope
@Component(
        modules = ActivityModule.class,
        dependencies = MainApplicationComponent.class
)
public interface ActivityComponent {
    void inject(BaseActivity baseActivity);
    void inject(BaseFragment baseFragment);
    void inject(BaseDialogFragment baseDialogFragment);

    Activity activity();
}
