package zambelz.library.core.components;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import zambelz.library.core.config.AppConfig;
import zambelz.library.core.modules.GsonModule;
import zambelz.library.core.modules.MainApplicationModule;
import zambelz.library.core.modules.NetworkModule;
import zambelz.library.core.modules.OkHttpClientModule;
import zambelz.library.core.modules.RxJavaModule;
import zambelz.library.core.utilities.SharedPreferenceLoader;

import static zambelz.library.core.modules.GsonModule.WITH_EXPOSED;

/**
 * Created by nandajulianda on 11/20/16.
 */

@Singleton
@Component(
        modules = {
                MainApplicationModule.class,
                OkHttpClientModule.class,
                GsonModule.class,
                RxJavaModule.class,
                NetworkModule.class
        }
)
public interface MainApplicationComponent {
    Context context();
    SharedPreferenceLoader sharedPreferenceLoader();
    SharedPreferences sharedPreferences();
    OkHttpClient okHttpClient();
    Cache cache();
    Gson gson();
    @Named(WITH_EXPOSED) Gson gsonWithExcludeSupport();
    GsonConverterFactory gsonConverterFactory();
    RxJavaCallAdapterFactory rxJavaCallAdapterFactory();
    Retrofit.Builder retrofitBuilder();
    Retrofit retrofit();
    AppConfig appConfig();
}
