package zambelz.library.core.utilities;

import java.text.DecimalFormat;

/**
 * Created by nandajulianda on 1/13/17.
 */

public final class CurrencyUtils {

    public static String format(Object object) {
        DecimalFormat df = new DecimalFormat("#,###.00");
        return df.format(object);
    }

}
