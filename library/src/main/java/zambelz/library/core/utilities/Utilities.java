package zambelz.library.core.utilities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import rx.Subscription;

/**
 * Created by nandajulianda on 11/19/16.
 */

public final class Utilities {

    private Utilities(){}

    public static int getResColor(Context context, int colorRes) {
        return ContextCompat.getColor(context, colorRes);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Drawable getLollipopDrawable(Context context, int drawableRes) {
        return context.getResources().getDrawable(drawableRes, null);
    }

    public static Drawable getDrawable(Context context, int drawableRes) {
        Drawable drawable = getLollipopDrawable(context, drawableRes);
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = ContextCompat.getDrawable(context, drawableRes);
        }

        return drawable;
    }

    public static int[] defaultSwipeColorRefresh() {
        return new int[] {
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light
        };
    }

    public static void unsubscribeSubscription(Subscription subscription) {
        if(subscription != null) {
            if(!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
    }

    public static boolean isPackageInstalled(Context context, String packagename) {
        try {
            context.getPackageManager()
                    .getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    @SuppressWarnings({"unchecked", "TryWithIdenticalCatches"})
    public static <T> T getClass(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String uppercaseFirstString(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

}
