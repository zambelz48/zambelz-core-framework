package zambelz.library.core.utilities;

import android.os.Environment;
import android.text.format.DateFormat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by nandajulianda on 11/29/16.
 */

public final class LoggerUtils {

    private LoggerUtils() {}

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static void writeErrorLog(String logDir, String log_text) {
        if(!isExternalStorageWritable()) {
            return;
        }

        File file = new File(Environment.getExternalStorageDirectory(), logDir);
        if(!file.exists()) {
            file.mkdirs();
        }

        String current_date = String.valueOf(DateFormat.format("yyyy-MM-dd", Calendar.getInstance().getTime()));
        String  time_created = String.valueOf(DateFormat.format("hh:mm:ss", Calendar.getInstance().getTime()));
        String  file_name = "error_"+ current_date +".log";

        try {
            File logFile = new File(file.getPath(), file_name);

            FileWriter writer = new FileWriter(logFile, true);
            writer.append("\n" + time_created + " | => " + log_text);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
