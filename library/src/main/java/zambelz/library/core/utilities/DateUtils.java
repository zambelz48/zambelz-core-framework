package zambelz.library.core.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by nandajulianda on 12/7/16.
 */

public class DateUtils {

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    public static long getElapsedHour(Date startDate, Date endDate){
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;

        return different / hoursInMilli;
    }

    private static SimpleDateFormat getFormatter(String format) {
        return new SimpleDateFormat(format, Locale.getDefault());
    }

    public static String getTimeAgo(String time) {
        SimpleDateFormat formatter = getFormatter("dd/MM/yyyy hh:mm:ss");
        Date date = new Date();
        try {
            date = formatter.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return getTimeAgo(date.getTime());
    }

    public static Date getDate(String dateStr, String format) {
        try {
            return getFormatter(format).parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Date getDate(Date date, String format) {
        try {
            SimpleDateFormat sdf = getFormatter(format);
            String dateStr = getFormatter(format).format(date);
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDateString(String dateStr, String format) {
        try {
            SimpleDateFormat sdf = getFormatter(format);
            Date date = sdf.parse(dateStr);
            return sdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDateString(Date date, String format) {
        return getFormatter(format).format(date.getTime());
    }

    public static String getDateString(Calendar calendar, String format) {
        return getFormatter(format).format(calendar.getTime());
    }

    public static String getDate(String dateStr, String sourceFormat, String targetFormat) {
        try {
            Date date = getFormatter(sourceFormat).parse(dateStr);
            return getFormatter(targetFormat).format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getCurrentDate(String format) {
        Date date = new Date();
        return getFormatter(format).format(date.getTime());
    }

    public static String getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String result = "";

        switch (day) {
            case Calendar.SUNDAY :
                result = "sunday";
                break;
            case Calendar.MONDAY :
                result = "monday";
                break;
            case Calendar.TUESDAY :
                result = "tuesday";
                break;
            case Calendar.WEDNESDAY :
                result = "wednesday";
                break;
            case Calendar.THURSDAY :
                result = "thursday";
                break;
            case Calendar.FRIDAY :
                result = "friday";
                break;
            case Calendar.SATURDAY :
                result = "saturday";
                break;
        }

        return result;
    }

}
