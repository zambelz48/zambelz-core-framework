package zambelz.library.core.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by nandajulianda on 12/28/16.
 */

public class SharedPreferenceLoader {

    private final Context context;

    public SharedPreferenceLoader(Context context) {
        this.context = context;
    }

    public SharedPreferences getCustomPreference(String file, int mode) {
        return context.getSharedPreferences(file, mode);
    }

    public SharedPreferences getCustomPreference(int file, int mode) {
        return getCustomPreference(context.getString(file), mode);
    }

    public SharedPreferences getDefaultPreference() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
